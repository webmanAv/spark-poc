package com.example.sparkpoc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.Map;

@SpringBootApplication
public class SparkPocApplication {
	static String sourceTopic = System.getenv("SOURCE_TOPIC");
	static String middleTopic = System.getenv("MIDDLE_TOPIC");
	static String targetTopic = System.getenv("TARGET_TOPIC");
	static String pulsarHost = System.getenv("PULSAR_HOST");
	static String pulsarUrl = "pulsar://" + pulsarHost +":6650";
	static String checkpointPath = System.getenv("CHECKPOINT_PATH");

	public static void main(String[] args) throws StreamingQueryException {

		SparkSession spark = SparkSession
				.builder()
				.config("spark.master", "local")
				.appName("Spark POC")
				.getOrCreate();

		Dataset<Row> stream = spark
				.readStream()
				.format("pulsar")
				.option("service.url", pulsarUrl)
				.option("admin.url", "http://localhost:6651")
				.option("topic", sourceTopic)
				.load();


		Dataset<String> msgList = stream.selectExpr("CAST(value AS STRING)").as(Encoders.STRING());

//		msgList.groupByKey(x->{
//			ObjectMapper objectMapper = new ObjectMapper();
//			Map map = objectMapper.readValue(x.toString(), Map.class);
//			return map.get("mac").toString();
//		}, Encoders.STRING()).flatMapGroupsWithState(()->{
//
//		})

		StreamingQuery query = msgList.writeStream()
				.outputMode("append")
				.format("console")
				.start();




		query.awaitTermination();
	}

}
