#!/bin/bash
docker run -d \
  -p 6650:6650 \
   -p 6651:8080 \
  --name pulsar \
  apachepulsar/pulsar:2.5.0 \
  bin/pulsar standalone


